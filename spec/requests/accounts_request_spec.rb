require 'rails_helper'

RSpec.describe "Accounts", type: :request do
  let!(:account) { Fabricate :account }
  let!(:account_balance) { Fabricate :account_balance, account: account }

  let!(:line_1) { Fabricate :line, created_at: 1.minute.ago, account: account }
  let!(:line_2) { Fabricate :line, created_at: 2.minute.ago, account: account }
  let!(:line_3) { Fabricate :line, created_at: 3.minute.ago, account: account }
  let!(:line_4) { Fabricate :line, created_at: 4.minute.ago, account: account }
  let!(:line_5) { Fabricate :line, created_at: 5.minute.ago, account: account }
  let!(:line_6) { Fabricate :line, created_at: 6.minute.ago, account: account }
  let!(:line_7) { Fabricate :line, created_at: 7.minute.ago, account: account }
  let!(:line_8) { Fabricate :line, created_at: 8.minute.ago, account: account }
  let!(:line_9) { Fabricate :line, created_at: 9.minute.ago, account: account }
  let!(:line_10) { Fabricate :line, created_at: 10.minute.ago, account: account }
  let!(:line_11) { Fabricate :line, created_at: 11.minute.ago, account: account }

  let(:top_10_recent_lines) do
    [
      line_1.as_json(only: [:amount, :created_at]),
      line_2.as_json(only: [:amount, :created_at]),
      line_3.as_json(only: [:amount, :created_at]),
      line_4.as_json(only: [:amount, :created_at]),
      line_5.as_json(only: [:amount, :created_at]),
      line_6.as_json(only: [:amount, :created_at]),
      line_7.as_json(only: [:amount, :created_at]),
      line_8.as_json(only: [:amount, :created_at]),
      line_9.as_json(only: [:amount, :created_at]),
      line_10.as_json(only: [:amount, :created_at]),
    ]
  end

  describe 'get /accounts' do
    it 'responds with account' do
      get "/accounts/#{account.id}"
      expect(response).to have_http_status(200)
      expect(response_body).to include(
        'name' => account.name,
        'account_balance' => { 'balance' => account_balance.balance },
        'recent_lines' => top_10_recent_lines )
    end
  end

  describe 'error cases' do
    context 'when account not exist' do
      it 'returns 404' do
        get "/accounts/boom"
        expect(response).to have_http_status(404)
      end
    end
  end
end
