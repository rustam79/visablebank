require 'rails_helper'

RSpec.describe "Transfers", type: :request do
  let!(:account_1) { Fabricate :account }
  let!(:account_2) { Fabricate :account }
  let!(:account_balance) { Fabricate :account_balance, account: account_1, balance: 50_00 }

  describe 'post /transfers' do
    let(:amount) { 10_00 }
    let(:transfer_params) do
      {
        transfer: {
          from: account_1.id,
          to: account_2.id,
          amount: amount,
        }
      }
    end

    it 'creates credit and debit lines' do
      expect{
        post '/transfers', params: transfer_params, as: :json
      }.to change{ Line.count }.by(2)
      .and change{ account_balance.reload.balance }.by(-amount)

      expect(response).to have_http_status :created
      expect(response_body).to include('status' => 'ok')
    end

    it 'allows multiple transfers at the same time' do
      expect do
        threads = [
          Thread.new{ post '/transfers', params: transfer_params, as: :json },
          Thread.new{ post '/transfers', params: transfer_params, as: :json },
          Thread.new{ post '/transfers', params: transfer_params, as: :json },
        ].each(&:join)

      end.to change{ Line.count }.by(6)
      .and change{ account_balance.reload.balance }.by(-amount*3)
    end
  end

end
