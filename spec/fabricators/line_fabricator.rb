Fabricator(:line) do
  amount Faker::Number.number(digits: 5)
  balance Faker::Number.number(digits: 10)
end
