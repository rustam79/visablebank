Fabricator(:account_balance) do
  balance Faker::Number.number(digits: 10)
end
