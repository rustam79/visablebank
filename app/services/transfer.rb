class Transfer
  # Responsible for transferring funds between accounts.
  #
  # max number of retries to commit transaction
  MAX_RETRIES = 5
  # delays between retries
  SLEEP_DURATION = 0.1

  # Response of Transfer.
  Response = Struct.new(:success, :credit, :debit, :message) do
    def self.success(*args)
      new(true, *args)
    end

    def self.error(*args)
      new(false, *args)
    end
  end

  attr_reader :amount, :from, :to

  # @param [Integer] a positive amount of money to transfer, in cents.
  # @param [Account] source account.
  # @param [Account] destination account.
  # @return [Transfer] initialized instance.
  def initialize(amount, from, to)
    @amount = amount
    @from = from
    @to = to
  end

  # Attempts to create credit and debit lines and create or update account balances
  # for `from` and `to` accounts.
  # Retries on lock exception from database up to +MAX_RETRIES+ times.
  #
  # @return [Response]
  def process
    raise AmountIsNegative if amount.negative?
    raise AccountsIdentical if from == to

    retries ||= 0
    credit, debit =
      AccountBalance.transaction do
        update_balances!
        create_lines!
      end
    Response.success(credit, debit)

  # When a parallel running transactions attempt to insert account balances for the same account
  # one transaction would fail with `StatementInvalid` exception which we'd rescue and retry here.
  #
  # Also when other transaction attempts to lock account balance which is already locked
  # it would fail with `LockWaitTimeout` exception.
  rescue ActiveRecord::LockWaitTimeout, ActiveRecord::StatementInvalid => e
    retries += 1
    if (retries <= MAX_RETRIES)
      sleep SLEEP_DURATION
      retry
    end
    Response.error(nil, nil, e.to_s)
  rescue TransferError => e
    Response.error(nil, nil, e.to_s)
  rescue ActiveRecord::RecordInvalid => e
    Response.error(nil, nil, e.full_message)
  end

  private

  # Updates or creates account balances in context of transfer.
  # @return [void]
  def update_balances!
    supplement_account_balance(from, -amount)
    supplement_account_balance(to, amount)
  end

  # Creates credit and debit lines in context of transfer.
  # @return [Array<Response>] credit and debit lines.
  def create_lines!
    credit, debit = Line.new, Line.new

    credit.amount, debit.amount   = -amount, amount
    credit.account, debit.account = from, to
    credit.balance, debit.balance = from.account_balance.balance, to.account_balance.balance

    credit.save!
    debit.save!

    [credit, debit]
  end

  # Adds given amount to account balance.
  #
  # @param [Account] to balance.
  # @param [Integer] cents.
  # @return [Balance] created or updated.
  def supplement_account_balance(account, amount)
    balance = account.account_balance
    if balance.present?
      # parallel transaction would raise exception if account is locked
      balance.lock!("FOR UPDATE NOWAIT")
      balance.update!(balance: balance.balance + amount)
    else
      balance = account.create_account_balance(balance: amount)
    end

    balance
  end

  class TransferError < RuntimeError; end
  class AmountIsNegative < TransferError; end
  class AccountsIdentical < TransferError; end
end
