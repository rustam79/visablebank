class Account < ApplicationRecord
  has_one :account_balance
  has_many :lines
  has_many :recent_lines, -> { order(created_at: :desc).limit(10) }, class_name: "Line"

end
