class Line < ApplicationRecord
  belongs_to :account

  validates :account, :amount, :balance, presence: true
  validates :amount, numericality: true

end
