class AccountBalance < ApplicationRecord
  belongs_to :account

  validates :account, :balance, presence: true
  # change to `numericality: true` to allow negative balances
  validates :balance, numericality: { greater_than_or_equal_to: 0 }

  def as_json(options={})
    {
      balance: balance,
    }
  end
end
