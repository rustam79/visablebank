class AccountsController < ApplicationController
  before_action :get_account

  def show
    render json: account_json
  end

  private

  def account_json
    @account.as_json(
      only: :name,
      include: {
        account_balance: { only: :balance },
        recent_lines: { only: [:amount, :created_at]}})
  end

  def get_account
    @account ||= Account.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    head 404
  end
end
