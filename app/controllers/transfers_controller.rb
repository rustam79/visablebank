class TransfersController < ApplicationController
  before_action :process_transfer, only: :create

  def create
    if @response.success
      render json: { status: 'ok' }, status: :created
    else
      render json: { status: @response.message }, status: :unprocessable_entity
    end
  end

  private

  def process_transfer
    @response ||= transfer.process
  end

  def transfer
    Transfer.new(transfer_params[:amount], from, to)
  end

  def from
    Account.find(transfer_params[:from])
  end

  def to
    Account.find(transfer_params[:to])
  end

  def transfer_params
    params.require(:transfer).permit(:amount, :from, :to)
  end
end
