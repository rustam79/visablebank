class CreateLines < ActiveRecord::Migration[6.0]
  def change
    create_table :lines do |t|
      t.references :account, null: false, foreign_key: true
      t.bigint :amount
      t.bigint :balance

      t.timestamps
    end

    add_index :lines, [:account_id, :id]
    add_index :lines, [:account_id, :created_at]
  end
end
