class CreateAccountBalances < ActiveRecord::Migration[6.0]
  def change
    create_table :account_balances do |t|
      t.references :account, null: false, foreign_key: true
      t.bigint :balance, null: false

      t.timestamps
    end
  end
end
