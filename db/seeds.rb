# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

account_1 = Account.create(name: 'John Dow')
account_2 = Account.create(name: 'Mary Smith')

AccountBalance.create(account: account_1, balance: 100_00)
AccountBalance.create(account: account_2, balance: 5_00)

13.times do
  Transfer.new(5_00, account_1, account_2).process
end

5.times do
  Transfer.new(3_00, account_2, account_1).process
end
