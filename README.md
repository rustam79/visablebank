# VisableBank application

This is my implementation of an accounting system based on the principles of a [Double-entry Bookkeeping](http://en.wikipedia.org/wiki/Double-entry_bookkeeping_system) system written with Ruby on Rails. The application performs basic banking operations like money transfers and showing of current account balance.

## Api provide the following actions
- get `/accounts/:id` - Show the balance of the account and 10 latest transactions.
- post `/transfers` - Transfer money between accounts. 

If transfer succeeds api returns a status code `200` response and body `status: ok`, otherwise it returns a status code `422` and `status` field in body would contain error message. The transfer payload must contain the following fields:`{ transfer: { from: from_account_id, to: to_account_id, amount: amount_to_transfer }}`

## Requirements
- Ruby 2.7.0 or above
- Rails 6
- Postgresql

## Setup
- clone this repo
- `gem install bunlder`
- `bundle install`
- `bundle exec rails db:create db:migrate db:seed`
- `bundle exec rspec` to run tests
- `rails s` to run api locally

## Notes

Negative amounts in balances are not allowed, however you can simply turn it on by changing validation rule in `AccountBalance` model.
